emacs --function org-beamer-export-to-pdf --file my-presentation.org

emacsclient -e '(progn (switch-to-buffer "my-presentation.org") (org-beamer-export-to-pdf))'

Great, I had to use find-file instead of switch-to-buffer
